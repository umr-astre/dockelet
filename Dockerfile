FROM ubuntu:latest

LABEL maintainer="Facundo Muñoz facundo.munoz@cirad.fr"

#ENV TZ="Europe/Paris"

# Non-root user for security purposes.
#
# UIDs below 10,000 are a security risk, as a container breakout could result
# in the container being ran as a more privileged user on the host kernel with
# the same UID.
#
# Static GID/UID is also useful for chown'ing files outside the container where
# such a user does not exist.
#RUN addgroup --gid 10001 --system ocelet \
# && adduser  --uid 10000 --system --ingroup ocelet --home /home/ocelet ocelet


## System dependencies #####################################
RUN apt-get update && apt-get install -y \
    openjdk-8-jdk \
    sudo xcompmgr pulseaudio \
  && rm -rf /var/lib/apt/lists/*


## Installation Ocelet #############################################
COPY omp210_linux64/ /omp210_linux64/
COPY entrypoint.sh /sbin/entrypoint.sh
RUN chmod 755 /sbin/entrypoint.sh

#RUN mkdir -p /home/ocelet/workspace

ENTRYPOINT ["/sbin/entrypoint.sh"]

#USER ocelet

#CMD ["/sbin/entrypoint.sh"]

