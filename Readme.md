<img src="dockelet.png" alt="dockelet logo" width="200"/>

Run Ocelet in a docker container, on any platform.

[Ocelet](http://www.ocelet.fr/en) is a modelling platform written in java.
It can be a bit picky about the java version in the system.
__dockelet__ is a convenient containerised image of Ocelet running on a Ubuntu container properly configured.

You simply need to have docker installed, pull the image and run the `dockelet` launcher.

# Set up

Install [docker](https://www.docker.com/get-started)


# Build image

Pull the docker image with ocelet

```bash
docker pull registry.forgemia.inra.fr/umr-astre/dockelet
```



# Install

Download the launcher script `dockelet` to where it can be found (e.g. `~/bin/`) and make sure it is executable.
E.g.

```bash
wget -P ~/bin https://forgemia.inra.fr/umr-astre/dockelet/-/raw/master/dockelet
chmod 755 ~/bin/dockelet
```

# Run Ocelet in a container

Create a directory `workspace` where you want your models stored. Move to its parent directory.
For instance, 
```bash
cd ~/work
mkdir workspace
```

Run!

```bash
./dockelet
```

Alternatively, you can set the environment variable $OCELET_WS to define the location of your workspace directory.
This is useful for making a menu launcher.
You can then define de launch command as 

```bash
OCELET_WS='~/work' ~/bin/dockelet
```

This is a script that will handle some technical details, such as defining some environmental variables needed to 
forward the graphical interfaces and passing your user IDs in order to keep file permissions right.

The script will print the final call in the console for you to check or modify if needed.

# DIY

If you want to build the image yourself, download or clone this repository in a temporary directory and run

```bash
wget http://ocelet.fr/ocws/files/rel/omp210_linux64.zip
unzip omp210_linux64.zip
docker build -t ocelet .
```
