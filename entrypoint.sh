#!/bin/bash
set -e

USER_UID=${USER_UID:-1000}
USER_GID=${USER_GID:-1000}

OCELET_USER=ocelet

create_user() {
  # create group with USER_GID
  if ! getent group ${OCELET_USER} >/dev/null; then
    groupadd -f -g ${USER_GID} ${OCELET_USER} >/dev/null 2>&1
  fi

  # create user with USER_UID
  if ! getent passwd ${OCELET_USER} >/dev/null; then
    adduser --disabled-login --uid ${USER_UID} --gid ${USER_GID} \
      --gecos 'OceletUs' ${OCELET_USER} >/dev/null 2>&1
  fi
  chown ${OCELET_USER}:${OCELET_USER} -R /home/${OCELET_USER}
  adduser ${OCELET_USER} sudo
}

grant_access_to_video_devices() {
  for device in /dev/video*
  do
    if [[ -c $device ]]; then
      VIDEO_GID=$(stat -c %g $device)
      VIDEO_GROUP=$(stat -c %G $device)
      if [[ ${VIDEO_GROUP} == "UNKNOWN" ]]; then
        VIDEO_GROUP=oceletusvideo
        groupadd -g ${VIDEO_GID} ${VIDEO_GROUP}
      fi
      usermod -a -G ${VIDEO_GROUP} ${OCELET_USER}
      break
    fi
  done
}

launch_ocelet() {
  # It is necessary to launch Ocelet from the directory where the program is installed *and*
  # where the workspace directory is installed.
  sudo -HEu ${OCELET_USER} cp -r /omp210_linux64/* /home/${OCELET_USER}/
  # sudo -HEu ${OCELET_USER} echo "alias ll='ls -lh --color=tty'" > /home/${OCELET_USER}/.bashrc  # debug
  cd /home/${OCELET_USER}
  exec sudo -HEu ${OCELET_USER} QT_GRAPHICSSYSTEM="native" ./omp
  #exec sudo -HEu ${OCELET_USER} bash  # debug
}

create_user
grant_access_to_video_devices
launch_ocelet
